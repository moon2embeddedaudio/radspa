#include "buffer.h"

radspa_descriptor_t buffer_desc = {
    .name = "buffer",
    .id = 22,
    .description = "forwards input signal to output signal",
    .create_plugin_instance = buffer_create,
    .destroy_plugin_instance = radspa_standard_plugin_destroy
};

void buffer_run(radspa_t * buffer, uint16_t num_samples, uint32_t render_pass_id){
    // note: this could be more lightweight by simply forwarding the buffer,
    // however at this point the radspa protocol has no built-in flag for this.
    // a host may still choose to simply do so to save CPU.
    // for the future, since this is a common use case, we should add some sort of
    // buffer forwarding flag. but it's okay. still lighter than the old approach
    // (running a single channel mixer).
    radspa_signal_t * output = radspa_signal_get_by_index(buffer, 0);
    radspa_signal_t * input = radspa_signal_get_by_index(buffer, 1);
    radspa_signal_copy(input, output, num_samples, render_pass_id);
}

radspa_t * buffer_create(uint32_t init_var){
    radspa_t * buffer = radspa_standard_plugin_create(&buffer_desc, 2, 0, 0);
    if(!buffer) return NULL;
    buffer->render = buffer_run;
    radspa_signal_set(buffer, 0, "output", RADSPA_SIGNAL_HINT_OUTPUT, 0);
    radspa_signal_set(buffer, 1, "input", RADSPA_SIGNAL_HINT_INPUT, 0);
    return buffer;
}
